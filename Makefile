# Configuration

CC		= gcc
LD		= gcc
AR		= ar
CFLAGS		= -g -std=gnu99 -Wall -Iinclude -fPIC
LDFLAGS		= -Llib
ARFLAGS		= rcs

# Variables

LIBRARY_HEADERS = $(wildcard include/pq/*.h)
LIBRARY_SOURCES = src/process.c src/process_list.c src/signal.c src/socket.c \
		  src/scheduler.c src/scheduler_internal.c \
		  src/scheduler_fifo.c src/scheduler_rdrn.c src/scheduler_mlfq.c \
		  src/server.c src/client.c
LIBRARY_OBJECTS	= $(LIBRARY_SOURCES:.c=.o)
STATIC_LIBRARY  = lib/libpq.a
PQ_PROGRAM	= bin/pq

TEST_SOURCES    = $(wildcard tests/test_*.c)
TEST_OBJECTS	= $(TEST_SOURCES:.c=.o)
TEST_PROGRAMS   = $(subst tests,bin,$(basename $(TEST_OBJECTS)))

# Rules

all:	$(PQ_PROGRAM)

%.o:	%.c $(LIBRARY_HEADERS)
	@echo "Compiling $@"
	@$(CC) $(CFLAGS) -c -o $@ $<

bin/%:	tests/%.o $(STATIC_LIBRARY)
	@echo "Linking $@"
	@$(LD) $(LDFLAGS) -o $@ $^

$(PQ_PROGRAM):	src/pq.o $(STATIC_LIBRARY)
	@echo "Linking $@"
	@$(LD) $(LDFLAGS) -o $@ $^

$(STATIC_LIBRARY):	$(LIBRARY_OBJECTS)
	@echo "Linking $@"
	@$(AR) $(ARFLAGS) $@ $^


test:	$(TEST_PROGRAMS)
	@for t in $(TEST_PROGRAMS); do 	\
	    echo "Testing $$t";		\
	    for i in $$(seq $$($$t 2>&1 | tail -n 1 | awk '{print $$1}')); do \
		$$t $$i;		\
	    done			\
	done

clean:
	@echo "Removing objects"
	@rm -f $(LIBRARY_OBJECTS) $(TEST_OBJECTS) src/*.o

	@echo "Removing static library"
	@rm -f $(STATIC_LIBRARY)

	@echo "Removing tests"
	@rm -f $(TEST_PROGRAMS)

	@echo "Removing pq"
	@rm -f $(PQ_PROGRAM)

.PRECIOUS: %.o
