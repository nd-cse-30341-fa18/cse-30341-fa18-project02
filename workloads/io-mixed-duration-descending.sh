#!/bin/sh

bin/pq add bin/worksim.py 0.1 10
bin/pq add bin/worksim.py 0.1 9
bin/pq add bin/worksim.py 0.1 8
bin/pq add bin/worksim.py 0.5 7
bin/pq add bin/worksim.py 0.5 6
bin/pq add bin/worksim.py 0.5 5
bin/pq add bin/worksim.py 0.9 4
bin/pq add bin/worksim.py 0.9 3
bin/pq add bin/worksim.py 0.9 2
bin/pq add bin/worksim.py 0.9 1

watch bin/pq status
