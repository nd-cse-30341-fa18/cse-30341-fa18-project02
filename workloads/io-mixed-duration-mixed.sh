#!/bin/sh

bin/pq add bin/worksim.py 0.1 1
bin/pq add bin/worksim.py 0.1 5
bin/pq add bin/worksim.py 0.1 30
bin/pq add bin/worksim.py 0.5 1
bin/pq add bin/worksim.py 0.5 5
bin/pq add bin/worksim.py 0.5 30
bin/pq add bin/worksim.py 0.9 1
bin/pq add bin/worksim.py 0.9 5
bin/pq add bin/worksim.py 0.9 30
bin/pq add bin/worksim.py 0.9 1

watch bin/pq status
