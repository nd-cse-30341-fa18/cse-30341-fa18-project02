/* process_list.h: PQ Process List */

#ifndef PQ_PROCESS_LIST_H
#define PQ_PROCESS_LIST_H

#include "pq/process.h"

#include <stdio.h>

/* Structure */

typedef struct ProcessList  ProcessList;

struct ProcessList {
    Process *head;  /* Head of list */
    Process *tail;  /* Tail of list */
    size_t   size;  /* Length of list */
};

/* Functions */

void        process_list_push(ProcessList *pl, Process *p);
Process *   process_list_pop(ProcessList *pl);
Process *   process_list_remove(ProcessList *pl, pid_t pid);
void        process_list_dump(ProcessList *pl, FILE *fs);

#endif

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
