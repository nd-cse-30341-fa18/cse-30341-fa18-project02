/* scheduler.h: PQ Scheduler */

#ifndef PQ_SCHEDULER_H
#define PQ_SCHEDULER_H

#include "pq/process_list.h"

/* Constants */

#define	MAX_LEVELS  8

typedef enum {
    FIFO_POLICY,                /* First in, first out */
    RDRN_POLICY,                /* Round robin */
    MLFQ_POLICY,                /* Multi-level feedback queue */
} Policy;

/* Structure */

typedef struct Scheduler Scheduler;

struct Scheduler {
    Policy	    policy;     /* Scheduling policy */
    size_t          ncpus;      /* Number of CPUs */
    size_t          nlevels;    /* Number of priority levels (MLFQ) */
    time_t          timeout;    /* Time slice (microseconds) */
    size_t          nprocesses; /* Total number of processes scheduled */

    ProcessList	    running;    /* List of running processes */
    ProcessList	    waiting;    /* List of waiting processes */

    /* List of priority levels (MLFQ) */
    ProcessList	    levels[MAX_LEVELS];

    /* Total turnaround and response time */
    double          total_turnaround_time;
    double          total_response_time;
};

/* Functions */

void    scheduler_next(Scheduler *s);
void    scheduler_wait(Scheduler *s);
void    scheduler_recv(Scheduler *s, int cfd);
void    scheduler_exit(Scheduler *s);

/* Policies */

void    scheduler_fifo(Scheduler *s);
void    scheduler_rdrn(Scheduler *s);
void    scheduler_mlfq(Scheduler *s);

#endif

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
