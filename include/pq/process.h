/* process.h: PQ Process */

#ifndef PQ_PROCESS_H
#define PQ_PROCESS_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/* Constants */

#define MAX_ARGUMENTS   1024

/* Structure */

typedef struct Process      Process;

struct Process {
    char    command[BUFSIZ];    /* Command to execute */
    pid_t   pid;                /* Process identifier (0 == invalid) */
    time_t  arrival_time;       /* Process arrival time (is placed into waiting queue) */
    time_t  start_time;         /* Process start time (is first placed into running queue) */

    int     priority;           /* MLFQ priority level */
    long    threshold;          /* MLFQ time allotment */

    bool    is_running;         /* Whether or not process is running (according to OS) */
    long    user_time;          /* Amount of time spent in user mode */
    long    kernel_time;        /* Amount of time spent in kernel mode */
    double  cpu_usage;          /* Percentage of CPU used */

    Process *next;              /* Pointer to next process */
};

/* Functions */

Process *   process_create(const char *command);
bool        process_start(Process *p);
bool        process_pause(Process *p);
bool        process_resume(Process *p);
bool        process_terminate(Process *p);
bool        process_poll(Process *p);

#endif

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
