/* server.h: PQ Server functions */

#ifndef PQ_SERVER_H
#define PQ_SERVER_H

#include "pq/scheduler.h"

int run_server(Scheduler *s, const char *ipc_path);

#endif

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
