/* signal.h: PQ Signal functions */

#ifndef PQ_SIGNAL_H
#define PQ_SIGNAL_H

#include <stdbool.h>
#include <signal.h>

/* Types */

#ifndef GNU_SOURCE
typedef void (*sighandler_t)(int);
#endif

/* Global Variables */

extern bool ShutdownScheduler;

/* Functions */

bool    signal_register(int signum, int flags, sighandler_t handler);
void    signal_mask(int signum, int mask);

void	sigchld_handler(int signum);
void	sigint_handler(int signum);

#endif

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
