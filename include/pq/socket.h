/* socket.h: PQ Socket functions */

#ifndef PQ_SOCKET_H
#define PQ_SOCKET_H

/* Functions */

int	socket_listen(const char *path);
int	socket_accept(int fd);
int	socket_connect(const char *path);
int	socket_poll(int fd, time_t timeout);

#endif

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
