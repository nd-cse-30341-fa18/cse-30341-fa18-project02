/* client.h: Client functions */

#ifndef PQ_CLIENT_H
#define PQ_CLIENT_H

int run_client(const char *ipc_path, const char *action, const char *command);

#endif

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
