/* test_process_list.c: Test Process List */

#include "pq/macros.h"
#include "pq/process_list.h"

#include <assert.h>

/* Constants */

Process PROCESSES[] = {
    { "1", 1 },
    { "2", 2 },
    { "3", 3 },
    { "4", 4 },
    { "0", 0 },
};

/* Test cases */

int test_00_process_list_push() {
    ProcessList pl = {0};
    size_t i;

    for (i = 0; PROCESSES[i].pid; i++) {
    	process_list_push(&pl, &PROCESSES[i]);
    	assert(pl.size == i + 1);
    }

    assert(pl.size == i);

    i = 0;
    for (Process *p = pl.head; p; p = p->next, i++) {
    	assert(streq(p->command, PROCESSES[i].command));
    	assert(p->pid == PROCESSES[i].pid);
    }

    return EXIT_SUCCESS;
}

int test_01_process_list_pop() {
    ProcessList pl = {0};
    size_t i, j;

    for (i = 0; PROCESSES[i].pid; i++) {
    	process_list_push(&pl, &PROCESSES[i]);
    }
    
    for (j = 0; PROCESSES[j].pid; j++) {
    	assert(pl.head);
    	Process *p = process_list_pop(&pl);
    	assert(p);
    	assert(streq(p->command, PROCESSES[j].command));
    	assert(p->pid == PROCESSES[j].pid);
    	assert(pl.size == i - j - 1);
    }
    assert(process_list_pop(&pl) == NULL);

    return EXIT_SUCCESS;
}

int test_02_process_list_remove() {
    ProcessList pl = {0};
    size_t i, j;

    for (i = 0; PROCESSES[i].pid; i++) {
    	process_list_push(&pl, &PROCESSES[i]);
    }
    
    for (j = 0; PROCESSES[j].pid; j++) {
    	assert(pl.head);
    	Process *p = process_list_remove(&pl, j + 1);
    	assert(p);
    	assert(streq(p->command, PROCESSES[j].command));
    	assert(p->pid == PROCESSES[j].pid);
    	assert(pl.size == i - j - 1);
    }
    assert(process_list_remove(&pl, 0) == NULL);

    return EXIT_SUCCESS;
}

/* Main execution */

int main(int argc, char *argv[]) {
    if (argc != 2) {
	fprintf(stderr, "Usage: %s NUMBER\n\n", argv[0]);
	fprintf(stderr, "Where NUMBER is right of the following:\n");
	fprintf(stderr, "    0. Test process_list_push\n");
	fprintf(stderr, "    1. Test process_list_pop\n");
	fprintf(stderr, "    2. Test process_list_remove\n");
	return EXIT_FAILURE;
    }

    int number = atoi(argv[1]);
    int status = EXIT_FAILURE;

    switch (number) {
	case 0:	status = test_00_process_list_push(); break;
	case 1:	status = test_01_process_list_pop(); break;
	case 2:	status = test_02_process_list_remove(); break;
	default:
	    fprintf(stderr, "Unknown NUMBER: %d\n", number);
	    break;
    }

    return status;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
