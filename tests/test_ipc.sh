#!/bin/sh

bin/pq add bin/worksim.py 0.1 1
bin/pq add bin/worksim.py 0.1 2
bin/pq add bin/worksim.py 0.1 3

bin/pq status  ; echo
bin/pq running ; echo
bin/pq waiting ; echo
bin/pq levels  ; echo

sleep 1
bin/pq status  ; echo

sleep 2
bin/pq status  ; echo

sleep 3
bin/pq status  ; echo

sleep 1
bin/pq flush   ; echo
