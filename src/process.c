/* process.c: Process */

#include "pq/macros.h"
#include "pq/process.h"

#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

/**
 * Create new process structure given command.
 * @param   command     String with command to execute
 * @return  New process structure
 */
Process *process_create(const char *command) {
    return NULL;
}

/**
 * Start process by forking and executing the command.
 * @param   p           Process structure
 * @return  Whether or not starting the process was successful
 */
bool process_start(Process *p) {
    return false;
}

/**
 * Pause process.
 * @param   p           Process structure
 * @return  Whether or not sending the signal was successful.
 **/
bool process_pause(Process *p) {
    return false;
}

/**
 * Resume process.
 * @param   p           Process structure
 * @return  Whether or not sending the signal was successful.
 **/
bool process_resume(Process *p) {
    return false;
}

/**
 * Terminate process.
 * @param   p           Process structure
 * @return  Whether or not sending the signal was successful.
 **/
bool process_terminate(Process *p) {
    return false;
}

/**
 * Poll process by updating status information (is_running, user_time,
 * kernel_time, cpu_usage) with informationf from /proc/uptime and
 * /proc/<pid>/stat.
 * @param   p           Process structure
 * @return  Whether or not sending the signal was successful.
 **/
bool process_poll(Process *p) {
    return false;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
