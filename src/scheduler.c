/* scheduler.c: Scheduler Public functions */

#include "pq/macros.h"
#include "pq/scheduler.h"

#include <errno.h>
#include <sys/wait.h>

/* Internal function prototypes */

void scheduler_add(Scheduler *s, FILE *fs, const char *command);
void scheduler_status(Scheduler *s, FILE *fs);
void scheduler_running(Scheduler *s, FILE *fs);
void scheduler_waiting(Scheduler *s, FILE *fs);
void scheduler_flush(Scheduler *s, FILE *fs);

/**
 * Schedule next process using appropriate policy.
 * @param   s	    Scheduler structure
 */
void scheduler_next(Scheduler *s) {
}

/**
 * Wait for any children and remove from running queue and update metrics.
 * @param   s	    Scheduler structure
 */
void scheduler_wait(Scheduler *s) {
}

/**
 * Receive and process client connection.
 * @param   s	    Scheduler structure
 * @param   cfd	    Client socket file descriptor
 */
void scheduler_recv(Scheduler *s, int cfd) {
}

/**
 * Exit scheduler by flushing queue and displaying status.
 * @param   s	    Scheduler structure
 */
void scheduler_exit(Scheduler *s) {
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
