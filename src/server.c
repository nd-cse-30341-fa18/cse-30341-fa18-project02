/* server.c */

#include "pq/macros.h"
#include "pq/scheduler.h"
#include "pq/signal.h"
#include "pq/socket.h"

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

/* Server */

int run_server(Scheduler *s, const char *ipc_path) {
    info("Starting Process Queue Server...");

    /* Register signal handlers */

    /* Listen on server socket */

    /* Loop until scheduler shutdown */
        /* Poll socket */

        /* Wait for any children */

        /* Schedule next process */

        /* Accept and process an incoming socket connection */

    /* Flush scheduler to stdout and display status */
    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
