/* process_list.c: Process List */

#include "pq/macros.h"
#include "pq/process_list.h"

#include <assert.h>
#include <errno.h>
#include <string.h>

/* Process List - Functions */

/* Push process to back of process list.
 * @param pl    Process list structure
 **/
void        process_list_push(ProcessList *pl, Process *p) {
}

/* Pop process from front of process list.
 * @param pl    Process list structure
 * @return  Process from front of process list
 **/
Process *   process_list_pop(ProcessList *pl) {
    return NULL;
}

/* Remove and return process with specified pid.
 * @param pl    Process list structure
 * @param pid   Pid of process to return
 * @return  Process from process list with specified pid
 **/
Process *   process_list_remove(ProcessList *pl, pid_t pid) {
    return NULL;
}

/**
 * Dump the contents of the process list to the specified stream.
 * @param pl    Process list structure
 * @param fs    Output file stream
 */
void process_list_dump(ProcessList *pl, FILE *fs) {
    fprintf(fs, "%6s %-24s %-8s %-8s %-9s %-8s %-10s %-10s\n", "PID", "COMMAND", "STATE", "USER", "THRESHOLD", "USAGE", "ARRIVAL", "START");
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
