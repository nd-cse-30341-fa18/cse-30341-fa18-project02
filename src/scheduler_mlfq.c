/* scheduler_mlfq.c: MLFQ Scheduler */

#include "pq/macros.h"
#include "pq/scheduler.h"

#include <assert.h>
#include <errno.h>
#include <sys/wait.h>
#include <unistd.h>

/**
 * Schedule next process using mlfq policy:
 *
 *  1. Preempt a running process
 *
 *  2. Apply periodic priority boost
 *
 *  3. Move processes from waiting queue to top priority level
 *
 *  4. Start or resume processes by moving from levels to running
 *
 * @param   s	    Scheduler structure
 */
void scheduler_mlfq(Scheduler *s) {
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
