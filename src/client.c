/* client.c: Client */

#include "pq/macros.h"
#include "pq/socket.h"

#include <errno.h>
#include <string.h>

/* Client */

int run_client(const char *ipc_path, const char *action, const char *command) {
    FILE *server_fs = NULL;
    int server_fd, result = EXIT_FAILURE;

    if ((server_fd = socket_connect(ipc_path)) < 0) {
        goto cleanup;
    }

    if ((server_fs = fdopen(server_fd, "w+")) == NULL) {
        error("Unable fdopen: %s", strerror(errno));
        goto cleanup;
    }
        
    if (streq(action, "status")) {
        fprintf(server_fs, "STATUS\n");
    } else if (streq(action, "running")) {
        fprintf(server_fs, "RUNNING\n");
    } else if (streq(action, "waiting")) {
        fprintf(server_fs, "WAITING\n");
    } else if (streq(action, "flush")) {
        fprintf(server_fs, "FLUSH\n");
    } else if (streq(action, "add")) {
        fprintf(server_fs, "ADD %s\n", command);
    } else {
        error("Unknown action: %s", action);
        goto cleanup;
    }
    
    char buffer[BUFSIZ];
    while (fgets(buffer, BUFSIZ, server_fs)) {
        fputs(buffer, stdout);
    }

    result = EXIT_SUCCESS;

cleanup:
    if (server_fs) fclose(server_fs);
    return result;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
