/* pq.cpp: Process Queue */

#include "pq/client.h"
#include "pq/macros.h"
#include "pq/scheduler.h"
#include "pq/server.h"

/* Parse command line options */

void usage(const char *program) {
    fprintf(stderr, "Usage: %s [options]\n\n", program);
    fprintf(stderr, "General Options:\n");
    fprintf(stderr, "    -h                 Print this help message\n");
    fprintf(stderr, "    -f PATH            Path to IPC channel\n");
    fprintf(stderr, "\nClient Options:\n");
    fprintf(stderr, "    add COMMAND        Add COMMAND to queue\n");
    fprintf(stderr, "    status             Query status of queue\n");
    fprintf(stderr, "    running            Query running jobs\n");
    fprintf(stderr, "    waiting            Query waiting jobs\n");
    fprintf(stderr, "    flush              Remove all jobs from queue\n");
    fprintf(stderr, "\nServer Options:\n");
    fprintf(stderr, "    -n NCPUS           Number of CPUs\n");
    fprintf(stderr, "    -p POLICY          Scheduling policy (fifo, rdrn, mlfq)\n");
    fprintf(stderr, "    -t MICROSECONDS    Time between scheduling\n");
}

bool parse_command_line_options(int argc, char *argv[], Scheduler *s, const char **ipc_path, const char **action, char *command) {
    int argind = 1;

    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
	char *arg = argv[argind++];
	char *opt = NULL;

	switch (arg[1]) {
	    case 'f':
	    	*ipc_path = argv[argind++];
	    	break;
	    case 'n':
	    	s->ncpus = atoi(argv[argind++]);
	    	break;
	    case 'p':
	        opt = argv[argind++];
	        if (streq(opt, "fifo")) {
	            s->policy = FIFO_POLICY;
                } else if (streq(opt, "rdrn")) {
	            s->policy = RDRN_POLICY;
                } else if (streq(opt, "mlfq")) {
	            s->policy = MLFQ_POLICY;
                } else {
                    fprintf(stderr, "Unknown policy: %s\n", opt);
                    return false;
                }
	    	break;
	    case 't':
	    	s->timeout = atoi(argv[argind++]);
	    	break;
	    case 'h':
	    	usage(argv[0]);
	    	return false;
	    default:
	    	usage(argv[0]);
	    	return false;
	}
    }

    if (argind < argc) {
        *action = argv[argind++];
    }

    while (argind < argc) {
        strcat(command, argv[argind++]);
        if (argind < argc)
            strcat(command, " ");
    }

    return true;
}

/* Main Execution */

int main(int argc, char *argv[]) {
    const char *ipc_path  = "pq.socket";
    const char *action    = NULL;
    Scheduler   scheduler = {
        FIFO_POLICY,
        1,
        MAX_LEVELS,
        250000,
        0
    };
    char command[BUFSIZ]  = "";

    if (!parse_command_line_options(argc, argv, &scheduler, &ipc_path, &action, command)) {
        return EXIT_FAILURE;
    }

    if (action == NULL || streq(action, "server")) {
        return run_server(&scheduler, ipc_path);
    } else {
        return run_client(ipc_path, action, command);
    }
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
