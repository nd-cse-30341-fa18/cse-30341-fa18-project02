/* socket.cpp: Unix Domain Sockets */

#include "pq/macros.h"
#include "pq/socket.h"

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <poll.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <unistd.h>

/* Socket - Listen */

int socket_listen(const char *path) {
    /* Create socket */
    int server_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (server_fd < 0) {
    	error("Unable to open socket %s: %s", path, strerror(errno));
    	return -1;
    }

    /* Remove old socket */
    unlink(path);

    /* Bind socket */
    struct sockaddr_un server_addr = { .sun_family = AF_UNIX };
    strcpy(server_addr.sun_path, path);
    socklen_t server_len = strlen(server_addr.sun_path) + sizeof(server_addr.sun_family);
    if (bind(server_fd, (struct sockaddr *)&server_addr, server_len) < 0) {
    	error("Unable to bind socket %s: %s", path, strerror(errno));
    	close(server_fd);
    	return -1;
    }

    /* Listen on socket */
    if (listen(server_fd, SOMAXCONN) < 0) {
    	error("Unable to listen on socket %s: %s", path, strerror(errno));
    	close(server_fd);
    	return -1;
    }

    return server_fd;
}

/* Socket - Accept */

int socket_accept(int fd) {
    struct sockaddr_un client_addr;
    socklen_t client_len = sizeof(struct sockaddr_un);
    return accept(fd, (struct sockaddr *)&client_addr, &client_len);
}

/* Socket - Connect */

int socket_connect(const char *path) {
    /* Create socket */
    int client_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (client_fd < 0) {
    	error("Unable to open socket %s: %s", path, strerror(errno));
    	return -1;
    }

    /* Connect to server */
    struct sockaddr_un server_addr = { .sun_family = AF_UNIX };
    strcpy(server_addr.sun_path, path);
    size_t server_len = strlen(server_addr.sun_path) + sizeof(server_addr.sun_family);
    if (connect(client_fd, (struct sockaddr *)&server_addr, server_len) < 0) {
    	error("Unable to connect to socket %s: %s", path, strerror(errno));
    	return -1;
    }

    return client_fd;
}

/* Socket - Poll */

int socket_poll(int fd, time_t timeout) {
    struct pollfd pfd = {fd, POLLIN|POLLPRI, 0};
    int result;

    if ((result = poll(&pfd, 1, timeout)) < 0 && errno != EINTR) {
	error("Unable to poll: %s", strerror(errno));
    }

    return result;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
