/* scheduler_internal.c: Scheduler Internal Functions */

#include "pq/macros.h"
#include "pq/scheduler.h"

#include <assert.h>
#include <errno.h>
#include <sys/wait.h>

/**
 * Add command to scheduler.
 * @param   s       Scheduler structure
 * @param   fs      Response file stream
 * @param   command Command string
 */
void scheduler_add(Scheduler *s, FILE *fs, const char *command) {
}

/**
 * Display list of running of processes.
 * @param   s       Scheduler structure
 * @param   fs      Response file stream
 **/
void scheduler_running(Scheduler *s, FILE *fs) {
}

/**
 * Display list of waiting of processes.
 * @param   s       Scheduler structure
 * @param   fs      Response file stream
 **/
void scheduler_waiting(Scheduler *s, FILE *fs) {
}

/**
 * Display list of processes in all MLFQ levels.
 * @param   s       Scheduler structure
 * @param   fs      Response file stream
 **/
void scheduler_levels(Scheduler *s, FILE *fs) {
}

/**
 * Display status of process queue.
 * @param   s       Scheduler structure
 * @param   fs      Response file stream
 */
void scheduler_status(Scheduler *s, FILE *fs) {
}

/**
 * Flush all queues in scheduler by removing all processes from running and
 * waiting.
 * @param   s       Scheduler structure
 * @param   fs      Response file stream
 */
void scheduler_flush(Scheduler *s, FILE *fs) {
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
