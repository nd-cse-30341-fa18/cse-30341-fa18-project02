/* scheduler_rdrn.c: Round Robin Scheduler */

#include "pq/macros.h"
#include "pq/scheduler.h"

#include <assert.h>
#include <errno.h>
#include <sys/wait.h>

/**
 * Schedule next process using round robin policy:
 *
 *  1. If there no waiting processes, then do nothing
 *
 *  2. Move one process from front of running queue and place in back of
 *  waiting queue
 *
 *  3. Move one process from front of waiting queue and place in back of
 *  running queue.
 *
 * @param   s	    Scheduler structure
 */
void scheduler_rdrn(Scheduler *s) {
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
